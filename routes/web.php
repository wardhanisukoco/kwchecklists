<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post(
    'auth/login', 
    [
       'uses' => 'AuthController@authenticate'
    ]
);

$router->group([
        'middleware' => 'jwt.auth',
        'prefix'     => 'checklists'
    ], function() use ($router) {

        $router->post("/", 'ChecklistController@store');
        $router->get("/{checklistId}", [
            'as'   => 'checklist.show',
            'uses' => 'ChecklistController@show'
        ]);
        $router->patch("/{checklistId}", 'ChecklistController@update');
        $router->delete("/{checklistId}", 'ChecklistController@destroy');

        $router->post("/{checklistId}/items", 'ItemController@store');
        $router->get("/{checklistId}/items/{itemId}", 'ItemController@show');
        $router->patch("/{checklistId}/items/{itemId}", 'ItemController@update');
        $router->delete("/{checklistId}/items/{itemId}", 'ItemController@destroy');
    }
);
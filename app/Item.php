<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model 
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    // protected $fillable = [
    //     'name', 'email',
    // ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'checklist_id',
    ];

    // public static function boot() {
    //     parent::boot();

    //     self::creating(function($model){
    //         $model->due = date("Y-m-d H:i:s", strtotime($model->due));
    //     });

    //     self::updating(function($model){
    //         $model->due = date("Y-m-d H:i:s", strtotime($model->due));
    //     });
    // }
}

<?php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;

use App\Checklist;
use App\Item;

class ChecklistController extends BaseController {

    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }
    // public function validate(Request $request, array $rules, array $messages, array $customAttributes) {

    //     $validator = Validator::make($request->all(), [
    //         'data'                          => 'required',
    //         'data.attributes'               => 'required',
    //         'data.attributes.object_domain' => 'required',
    //         'data.attributes.object_id'     => 'required',
    //         'data.attributes.description'   => 'required',
    //     ]);

    //     if ($validator->fails()) {
    //         return response($validator->errors());
    //     }

    // }
    public function store() {

        $rules = [
            'data'                          => 'required',
            'data.attributes'               => 'required',
            'data.attributes.object_domain' => 'required',
            'data.attributes.object_id'     => 'required',
            'data.attributes.description'   => 'required',
        ];

        $this->validate($this->request, $rules);

        $attributes   = $this->request->data["attributes"];
        $items        = $this->request->data["attributes"]["items"];

        unset($attributes["items"]);

        $checklist = Checklist::create($attributes);
        
        $items     = array_map(function($item) use($checklist) {
            return [
                "description"  => $item,
                "checklist_id" => $checklist->id,
                "created_at"   => date("Y-m-d H:i:s"),
            ];
        }, $items);

        $items     = Item::insert($items);

        return $this->show($checklist->id);; 
    }
    
    public function show($checklistId) {

        $checklist = Checklist::findOrFail($checklistId);

        $response = [
            "data" => [
                "type"       => "checklists",
                "id"         => $checklist->id,
                "attributes" => $checklist->toArray(),
            ],
            "links" => [
                "self" => route('checklist.store', ["checklistId" => $checklistId]),
            ],
        ];

        return response()->json($response); 
    }
    public function update($checklistId) {

        $rules = [
            'data'                          => 'required',
            'data.type'                     => 'required',
            'data.id'                       => 'required',
            'data.attributes'               => 'required',
            'data.attributes.object_domain' => 'required',
            'data.attributes.object_id'     => 'required',
            'data.attributes.description'   => 'required',
        ];

        $this->validate($this->request, $rules);
        
        $items      = $this->request->data["attributes"]["items"];
        $attributes = $this->request->data["attributes"];

        unset($attributes["items"]);

        $checklist = Checklist::findOrFail($checklistId);
        $checklist->fill($attributes);

        if (!$checklist->save()) {
            return response('', 404);
        }

        return $this->show($checklistId);
    }
    public function destroy($checklistId) {

        Checklist::findOrFail($checklistId)->delete(); 

        return response("", 204);
    }
}

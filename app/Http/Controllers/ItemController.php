<?php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;

use App\Checklist;
use App\Item;
use App\Http\Controllers\ChecklistController;

class ItemController extends BaseController {

    private $request;
    private $rules;

    public function __construct(Request $request) {

        $this->request = $request;
         
        $this->rules = [
            'data'                         => 'required',
            'data.attribute'               => 'required',
            'data.attribute.description'   => 'required',
        ];
    }
    public function store($checklistId) {
       
        $this->validate($this->request, $this->rules);

        $attributes = $this->request->data["attribute"];
        $checklist  = Checklist::findOrFail($checklistId);

        $attributes["checklist_id"] = $checklistId;

        $item = Item::create($attributes);

        return $this->show($checklistId, $item->id);
    }
    
    public function show($checklistId, $itemId) {

        $checklist = Checklist::findOrFail($checklistId);
        $item      = Item::findOrFail($itemId);

        $response = [
            "data" => [
                "type"       => "checklists",
                "id"         => $checklist->id,
                "attributes" => $item->toArray(),
            ],
            "links" => [
                "self" => route('checklist.show', ["checklistId" => $checklistId]),
            ],
        ];

        return response()->json($response); 
    }
    public function update($checklistId, $itemId) {

        $this->validate($this->request, $this->rules);
        
        $attributes = $this->request->data["attribute"];
        $checklist  = Checklist::findOrFail($checklistId);

        $attributes["checklist_id"] = $checklistId;

        $item = Item::findOrFail($itemId);
        $item->fill($attributes);

        if (!$item->save()) {
            return response('', 404);
        }

        return $this->show($checklistId, $itemId);
    }

    public function destroy($checklistId, $itemId) {

        $checklist  = Checklist::findOrFail($checklistId);
        Item::findOrFail($itemId)->delete();

        return response("", 204);
    }
}

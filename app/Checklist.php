<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DateTime;

class Checklist extends Model 
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    // protected $fillable = [
    //     'name', 'email',
    // ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'id',
    ];
    // protected $dateFormat = DateTime::ATOM;

    public function getUpdatedAtAttribute($date)
    {
        if (!is_null($date))
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(DateTime::ATOM);
        return $date;
    }
    public function getCreatedAtAttribute($date)
    {
        if (!is_null($date))
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(DateTime::ATOM);
        return $date;
    }
    public function getDueAttribute($date)
    {
        if (!is_null($date))
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format(DateTime::ATOM);
        return $date;
    }
    public function setUpdatedAtAttribute($date)
    {
        if (is_string($date))
            $this->attributes["updated_at"] = \Carbon\Carbon::createFromFormat(DateTime::ATOM, $date)->format('Y-m-d H:i:s');
    }
    public function setCreatedAtAttribute($date)
    {
        $this->attributes["created_at"] = $date;

        if (is_string($date))
            $this->attributes["created_at"] = \Carbon\Carbon::createFromFormat(DateTime::ATOM, $date)->format('Y-m-d H:i:s');
    }
    public function setDueAttribute($date)
    {
         $this->attributes["due"] = \Carbon\Carbon::createFromFormat(DateTime::ATOM, $date)->format('Y-m-d H:i:s');
    }
}
